import React, { Component } from "react";
import "./App.css";
// import ReactCSSTransitionGroup from "react-addons-css-transition-group";
//import FaClose from "react-icons/lib/ti/delete";
//import FaNext from "react-icons/lib/ti/arrow-right";
//import FaPrev from "react-icons/lib/ti/arrow-left";

/*
const images = [
  {
    id: 0,
    src:
      "https://freephotos.cc/storage/path/rYRz3w9uGt8SXWzgVGDcE2P4cPVKzetlUwoGIyVN.jpeg"
  },
  {
    id: 1,
    src:
      "https://freephotos.cc/storage/path/aq8jVXU0SKXYoMfMz7zRT2mwHPQqDn7nahwuGSmv.jpeg"
  },
  {
    id: 2,
    src: "https://images.pexels.com/photos/672358/pexels-photo-672358.jpeg"
  },
  {
    id: 3,
    src:
      "https://freephotos.cc/storage/path/DHnnqHNkpdjT1vDkVaRYrneM6WPmMRfIK5hEaHwI.jpeg"
  },
  {
    id: 4,
    src:
      "https://freephotos.cc/storage/path/xbJxjNgqZcGzzzl93kMcjpkXA3bTf4KUq71IVy5y.jpeg"
  },
  {
    id: 5,
    src:
      "https://freephotos.cc/storage/path/KUHRya2ECX0mmf4bP5UvLp6oNXWED8noJMrKmptg.jpeg"
  },
  {
    id: 6,
    src:
      "https://freephotos.cc/storage/path/rTurdxqZMBIecZuwvaL3x4yOptv7wnbXitbl0bSD.jpeg"
  },
  {
    id: 7,
    src:
      "https://freephotos.cc/storage/path/0CbTOkVQwlclGPEmUYjAeBmLWXwGrahD8d2JMgPh.jpeg"
  },
  {
    id: 8,
    src:
      "  https://freephotos.cc/storage/path/IRquU3u3hhuNCpxPUvzSK7UUdErZMFxyYTztt9a9.jpeg"
  },
  {
    id: 9,
    src:
      "https://freephotos.cc/storage/path/T3hW4rEkhEMovPsse0wO43gW1TznIciX7le5erxR.jpeg"
  },
  {
    id: 10,
    src:
      "https://freephotos.cc/storage/path/G7b7xvgryHQcnMZDYLN8Z5VJJJa14PMPJSnin2V4.jpeg"
  },
  {
    id: 11,
    src:
      "https://freephotos.cc/storage/path/N6oTQPJsf8XhHwElknpA5Re0yMVSCoSSTvSulREv.jpeg"
  }
];
*/

const IMAGE_POSITION = {
  FIRST: "First",
  LAST: "Last",
  NULL: "Null"
};

class Modal extends Component {
  escFunction = event => {
    if (event.keyCode === 27 && this.props.show) {
      this.props.handleClose();
    }
  };

  componentDidMount() {
    document.addEventListener(
      "keydown",
      this.escFunction,

      false
    );
  }
  componentWillUnmount() {
    document.removeEventListener(
      "keydown",
      this.escFunction,

      false
    );
  }
  render() {
    const openCloseClassName = this.props.show
      ? "modal display-block"
      : "modal display-none";

    return (
      <div className={openCloseClassName}>
        <div className="modal-overlay" onClick={this.props.handleClose} />

        <div className="modal-container">
          <div className="gallery-card animated fadeIn">
            <Image
              className="gallery-modal "
              image={this.props.image}
              onClick={null}
            />
          </div>
        </div>
        <div className="modal-close">
          <input
            className="fa-icon"
            value="Close"
            type="button"
            onClick={this.props.handleClose}
          />
        </div>

        <div className="modal-previous">
          {this.props.imagePosition !== IMAGE_POSITION.FIRST ? (
            <input
              className="fa-icon"
              type="button"
              value="Previous"
              onClick={this.props.handlePrevious}
            />
          ) : null}
        </div>
        <div className="modal-next">
          {this.props.imagePosition !== IMAGE_POSITION.LAST ? (
            <input
              className="fa-icon"
              type="button"
              value="Next"
              onClick={this.props.handleNext}
            />
          ) : null}
        </div>
      </div>
    );
  }
}

class Image extends Component {
  render() {
    const { className, onClick, url, name } = this.props;
    return (
      <img
        className={className}
        src={`http://localhost:1337/${url}`}
        alt={name}
        onClick={onClick}
      />
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedImage: 0,
      show: false,
      images: []
    };
  }

  componentDidMount() {
    this.fetchData();
  }

  componentWillMount() {
    this.fetchData();
  }

  fetchData = () => {
    fetch("http://localhost:1337/images")
      .then(response => response.json())
      .then(data => this.setState({ images: data }));
  };
  openModal = imageId => {
    this.setState({ selectedImage: imageId, show: true });
  };

  closeModal = () => {
    this.setState({ show: false });
  };

  handleNext = () => {
    this.setState({ selectedImage: this.state.selectedImage + 1 });
  };

  handlePrevious = () => {
    this.setState({ selectedImage: this.state.selectedImage - 1 });
  };

  getPosition = () => {
    // if (this.state.selectedImage === 0) {
    //   return IMAGE_POSITION.FIRST;
    // } else if (this.state.selectedImage === images.length - 1) {
    //   return IMAGE_POSITION.LAST;
    // } else {
    //   return IMAGE_POSITION.NULL;
    // }

    //Equivaut a

    return this.state.selectedImage === 0
      ? IMAGE_POSITION.FIRST
      : this.state.selectedImage === this.state.images.length - 1
      ? IMAGE_POSITION.LAST
      : IMAGE_POSITION.NULL;
  };

  render() {
    console.log(this.state.images);
    const { images } = this.state;
    return (
      <div>
        <div className="gallery-container">
          <h3>PHOTO GALLERY</h3>
          <hr />
          <div>
            <div>
              {images.map((image, index) => (
                <Image
                  className="gallery-thumbnail"
                  key={index}
                  name={image.name}
                  url={image.url.url}
                  onClick={() => this.openModal(image.id)}
                />
              ))}
            </div>
          </div>
          <hr />
          <h6>FABIEN NGUYEN - WEB DEVELOPER</h6>
        </div>

        <Modal
          show={this.state.show}
          handleClose={this.closeModal}
          openModal={this.props.openModal}
          image={images[this.state.selectedImage]}
          handlePrevious={this.handlePrevious}
          handleNext={this.handleNext}
          imagePosition={this.getPosition()}
        />
      </div>
    );
  }
}

export default App;
